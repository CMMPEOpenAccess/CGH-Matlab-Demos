% Close all the open figures
close all;

% Clear the currently defined variables
clear;

% Resolution along each edge
n = 512;

% Read in the target image and convert to greyscale
target = complex(double(rgb2gray(imread('lenna.png'))));

% Rescale the target. Mainly due to Matlab weirdness.
target = target / sqrt(mean(mean(abs(target).^2)));

% The initial replay field is just the target image
replay = target;

% Randomise the phase of the replay field because the eye is phase insensitive
% and it reduces edge enhancement
replay = replay.*exp(2.*pi.*1i.*rand(n,n));

% Do the algorithm 10 times
for i=1:10
    % 1) Back project current replay field 
    diffraction=ifftshift(ifft2(ifftshift(replay)))*n;
    
    % 2) Apply the SLM constraints - ie Modulate or Quantise
    diffraction=exp(1i*angle(diffraction));
    
    % 3) Forward project the current SLM values
    replay=fftshift(fft2(fftshift(diffraction)))/n;
    
    % 4) Apply replay field constraints - i.e Impose target magnitude
    replay=abs(target).*exp(1i*angle(replay));
end

% Run steps 1-3 again
diffraction=ifftshift(ifft2(ifftshift(replay)))*n;
diffraction=exp(1i*angle(diffraction));
replay=fftshift(fft2(fftshift(diffraction)))/n;

% Show the SLM phases
figure;
imshow(angle(diffraction),[-pi pi]);
title("SLM Phases");

% Show the replay field magnitudes
figure;
imshow(abs(replay),[])
title("Replay Field Magnitudes");
